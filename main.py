import json
import mysql.connector
from db.mysql_storage import MySQLDb
from db.event_log_storage import EventLogDb
from db.comment_storage import CommentDb
from db.user_storage import UserDb
from validators.length_validator import LengthValidator
from validators.bad_word_validator import BadWordValidator
from validators.input_validator import InputValidator
from validators.composite_valid import Composite
from filters.input_filter import InputFilter
from filters.trim_filter import TrimFilter
from filters.upper_filter import UpperFilter
from notificators.notify_in_log import NotifyInLog
from notificators.composite_notify import CompositeNotify
from app.app import App
from flask import Flask
from routers.router import TaskRoutes

# Подключение к базе данных
with open("c:/Users/zoshc/workspace/git_lab/todolist/data/mysql_config.json", 'r') as f:
    config = json.load(f)

db_con = mysql.connector.connect(host=config['host'],
                                 user=config['user'],
                                 password=config['password'],
                                 database=config['database'],
                                 port=config['port'])


db = MySQLDb(db_con)
els = EventLogDb(db_con)
cmnt = CommentDb(db_con)
user = UserDb(db_con)

notify1 = NotifyInLog(els)
notify = CompositeNotify([notify1])

task_validator = InputValidator()
task_validator.add('title', LengthValidator(5, 30), BadWordValidator())
task_validator.add('description', LengthValidator(20, 100), BadWordValidator())

task_filter = InputFilter()
task_filter.add('title', TrimFilter(), UpperFilter())

task_filter.add('description', TrimFilter())
task_filter.add('description', UpperFilter())


comment_filter = InputFilter()
comment_filter.add('message', TrimFilter(), UpperFilter())

user_filter = InputFilter()
user_filter.add('name', TrimFilter(), UpperFilter())
user_filter.add('surname', TrimFilter(), UpperFilter())

app = App(db, cmnt, user, els, task_validator, task_filter, comment_filter,  user_filter, notify)
router = Flask(__name__)
task_routes = TaskRoutes(app, router)


if __name__ == '__main__':
    router.run(debug=False, port=8000)
