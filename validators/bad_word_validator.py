
from interfaces.validator_interface import Validator


class BadWordValidator(Validator):
    lst_bad_words = ['fool']
        
    def is_valid(self,  value: str) -> tuple[bool, str]:
        for s in self.lst_bad_words:
            if value.find(s) != -1:
                return False, 'has bad word'
            return True, ''