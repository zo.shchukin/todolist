from interfaces.validator_interface import Validator


class InputValidator(Validator):
    def __init__(self):
        self.validators = {}

    def is_valid(self, obj):
        ok = True
        msg = ''
        for field, validators in self.validators.items():
            for validator in validators:
                value = getattr(obj, field)
                valid, err = validator.is_valid(value)
                if not valid:
                    msg += f'{field} {err}; '
                    ok = False
        return ok, msg

    def add(self, field, *validators):
        if field not in self.validators:
            self.validators[field] = []
        for v in validators:
            self.validators[field].append(v)