from interfaces.validator_interface import Validator


class LengthValidator(Validator):
    def __init__(self, min_length=5, max_length=60):
        self._min_length = min_length
        self._max_length = max_length

    def is_valid(self, value: str) -> tuple[bool, str]:
        if self._min_length <= len(value) < self._max_length:
            return True, ''
        else:
            return False, 'length wrong'
