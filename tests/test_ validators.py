import pytest

from validators.length_validator import LengthValidator
from validators.bad_word_validator import BadWordValidator


def test_length_validator():
    lv = LengthValidator(5, 15)
    assert lv.is_valid("add new") == (True, '')


def test_length_validator_bad():
    lv = LengthValidator(5, 15)
    assert lv.is_valid("add") == (False, 'length wrong')


def test_bad_word_validator():
    bw = BadWordValidator()
    assert bw.is_valid("add new") == (True, '')


def test_bad_word_validator_bad():
    bw = BadWordValidator()
    assert bw.is_valid("add validator fool") == (False, 'has bad word')


if __name__ == '__main__':
    pytest.main()
