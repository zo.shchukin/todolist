import pytest

from filters.trim_filter import TrimFilter


def test_filter():
    assert TrimFilter.filter('    hello  world  ') == 'hello world'


def test_filter_bad():
    assert TrimFilter.filter('    he llo  world  ') == 'he llo world'


if __name__ == '__main__':
    pytest.main()
