from interfaces.filter_interface import Filter


class CompositeFilter(Filter):

    def __init__(self, filters: list):
        self.filters = filters

    def filter(self, value):

        for f in self.filters:
            value = f.filter(value)

        return value
