import re
from interfaces.filter_interface import Filter


class TrimFilter(Filter):
    @staticmethod
    def filter(value: str, **kwargs) -> str:
        value = value.strip()
        return re.sub(' +', ' ', value)