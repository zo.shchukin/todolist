from interfaces.filter_interface import Filter


class InputFilter(Filter):
    def __init__(self):
        self.filters = {}

    def filter(self, obj):
        for field, filters in self.filters.items():
            for filter in filters:
                value = filter.filter(getattr(obj, field))
                setattr(obj, field, value)

    def add(self, field, *filters):
        if field not in self.filters:
            self.filters[field] = []
        for f in filters:
            self.filters[field].append(f)