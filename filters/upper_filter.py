from interfaces.filter_interface import Filter


class UpperFilter(Filter):

    @staticmethod
    def filter(value: str):
        return value.capitalize()
